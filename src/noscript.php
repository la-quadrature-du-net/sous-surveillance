<?php
   $css = array('vendor', 'app');
   $css = array_map(function($css_file){
     $files = glob("styles/$css_file*.css");
     $files = array_combine($files, array_map("filemtime", $files));
     reset(arsort($files));
     return key($files);
   }, $css);

   $position = intval($_GET['position']);
   $page = intval($_GET['page']);
   $all = intval($_GET['all']);

   if (!$page) {
     $page = 1;
   }
   if ($position) {
     $all = 1;
   }
   $nppage = 50;

   $deputes = file_get_contents('assets/data/deputes_en_mandat.json');
   $deputes_infos = file_get_contents('assets/data/deputes_infos.json');
   $deputes_data = json_decode($deputes);
   $deputes_infos = json_decode($deputes_infos);
   $deputes = array();
   foreach($deputes_data->deputes as $depute){
     $slug = $depute->depute->slug;
     $depute->infos = $deputes_infos->$slug;
     $depute->color = ($depute->infos->position == '2') ? 'red-mask' : 'green-mask';
     if (!$position or $position == $depute->infos->position)
        $deputes[] = $depute;
   }
?>

<!DOCTYPE html>
<html ng-app="soussurveillance">

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <style type="text/css">
    @charset "UTF-8";
    [ng\:cloak],
    [ng-cloak],
    [data-ng-cloak],
    [x-ng-cloak],
    .ng-cloak,
    .x-ng-cloak,
    .ng-hide:not(.ng-hide-animate) {
      display: none !important;
    }

    ng\:form {
      display: block;
    }
    .depute-image-block{
      border-bottom: 1px solid #ccc;
      margin-top: 15px;
      width: 400px;
      height: 220px;
    }
    .depute-image{
      float: left;
    }
    .depute-infos-static{
      margin-left: 170px;
    }

    /*.red-mask {
      background: #446CB3 !important;
    }

    .green-mask {
      background: #F7CA18 !important;
    }*/

  </style>
  <meta charset="utf-8">
  <title>Projet de loi relatif au Renseignement : agissons contre la surveillance de masse ! | Sous-Surveillance.fr</title>
  <meta name="description" content="Projet de loi relatif au Renseignement, loi Renseignement, Surveillance de masse des citoyens, Légalisation de pratiques illégales, Pas de contrôle des services de renseignement, French Surveillace State">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" type="image/png" href="https://sous-surveillance.fr/favicon.png">
  <?php
    foreach($css as $css_file){
      echo '<link rel="stylesheet" href="'.$css_file.'">';
    }
  ?>
</head>

<body>
  <div>
    <div class="jumbotron text-center ">
      <h1>
        <img src="assets/images/oeil.png" alt="" style="width: 250px; position: relative;"> Projet de loi Renseignement</h1>
      <h2>Légaliser la surveillance totale ?
        <strong>NON !</strong>
      </h2>
    </div>
    <div>
      <ul class="sharing-buttons ">
        <li>
          <a class="facebook" href="https://www.facebook.com/sharer.php?u=http://sous-surveillance.fr/" target="_blank">
            <img alt="" onload="this.style.opacity=1;" src="assets/share/facebook.png" style="opacity: 1;">
            <span class="text">&nbsp; Share on Facebook</span>
          </a>
        </li>
        <li>
          <a class="twitter" href="https://twitter.com/intent/tweet?related=laquadrature&amp;text=Appelez%20vos%20d%C3%A9put%C3%A9s%20%21%20Refusez%20cette%20loi%20de%20surveillance%20%21%20sous-surveillance.fr%20%23PJLRenseignement" target="_blank">
            <img alt="" onload="this.style.opacity=1;" src="assets/share/twitter.png" style="opacity: 1;">
            <span class="text">&nbsp; Share on Twitter</span>
          </a>
        </li>
        <li>
          <a class="tumblr" href="http://www.tumblr.com/share/link?url=http%3A%2F%2Fsous-surveillance.fr" target="_blank">
            <img alt="" onload="this.style.opacity=1;" src="assets/share/tumblr.png" style="opacity: 1;">
            <span class="text">&nbsp; Share on Tumblr</span>
          </a>
        </li>
        <li>
          <a class="pressthis" href="http://www.addtoany.com/add_to/wordpress?linkurl=http%3A%2F%2Fsous-surveillance.fr&amp;linkname=Sous%20Surveillance&amp;linknote=Appelez%20vos%20d%C3%A9put%C3%A9s%20%21%20Refusez%20cette%20loi%20de%20surveillance%20%21%20sous-surveillance.fr%20%23PJLRenseignement"
          target="_blank">
            <img alt="" onload="this.style.opacity=1;" src="assets/share/pressthis.png" style="opacity: 1;">
            <span class="text">&nbsp; Press This</span>
          </a>
        </li>
        <li>
          <a class="googleplus" href="https://plus.google.com/share?url=http%3A%2F%2Fsous-surveillance.fr" target="_blank">
            <img alt="" onload="this.style.opacity=1;" src="assets/share/google-plus.png" style="opacity: 1;">
            <span class="text">&nbsp; Share on Google+</span>
          </a>
        </li>
        <li>
          <a class="diaspora" href="http://sharetodiaspora.github.io/?title=Appelez%20vos%20d%C3%A9put%C3%A9s%20%21%20Refusez%20cette%20loi%20de%20surveillance%20%21%20sous-surveillance.fr%20%23PJLRenseignement&amp;url=http%3A%2F%2Fsous-surveillance.fr"
          target="_blank">
            <img src="assets/share/diaspora.png" style="border: 0px solid;">
          </a>
        </li>
      </ul>
    </div>
    <div id="main" class="container-fluid">
      <!-- ngInclude: 'app/main/components/countdown.html' -->
      <div>
        <div id="countdown">Plus que quelques jours pour agir contre la surveillance généralisée&nbsp;!</div>
      </div>
      <!-- ngInclude: 'app/main/components/description/description.html' -->
      <div class="container">
        <h2>Le projet de loi relatif au Renseignement met en danger les libertés fondamentales.</h2>
        <h3>Quels problèmes pose-t-il ?</h3>
        <div id="description-block" ng-controller="DescriptionCtrl">
          <accordion close-others="false">
            <div class="panel-group" ng-transclude="">
              <div class="panel panel-default ng-isolate-scope">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="" class="accordion-toggle" ng-click="toggleOpen()" accordion-transclude="heading">
                      <span>Surveillance de masse des citoyens </span>
                      <span style="float:right" class="glyphicon glyphicon-plus " aria-hidden="true"></span>
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse">
                  <div class="panel-body" ng-transclude="">
                    <accordion-heading></accordion-heading>
                    <p>Le projet de loi Renseignement contient deux articles qui permettent une interception de l'ensemble des données de tous les citoyens français en temps réel sur Internet, dans le but de faire tourner dessus des outils de détection des
                      comportements « suspects ». Cette surveillance massive de l'ensemble de la population est inadmissible&nbsp;: c'est une pratique dangereuse pour la démocratie et les libertés d'expression, de réunion, de pensée, d'action.</p>
                    <p>
                      <strong>Nous ne voulons pas d'une copie de la NSA en France !</strong>
                    </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default ng-isolate-scope">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="" class="accordion-toggle" ng-click="toggleOpen()" accordion-transclude="heading">
                      <span>Pas de contrôle des services de renseignement </span>
                      <span style="float:right" class="glyphicon glyphicon-plus " aria-hidden="true"></span>
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse">
                  <div class="panel-body" ng-transclude="">
                    <accordion-heading></accordion-heading>
                    <p>Le projet de loi était prévu pour être une « loi d'encadrement du Renseignement ». En réalité, sur bien des points le contrôle est inexistant&nbsp;: la commission de contrôle n'a qu'un avis consultatif, le recours effectif des citoyens
                      contre les services de renseignement sont inapplicables, aucune sanction n'est prévue pour les agents qui abuseraient de leur pouvoir.</p>
                    <p>
                      <strong>Notre démocratie doit garantir des contre-pouvoirs forts pour protéger les citoyens !</strong>
                    </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default ng-isolate-scope">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="" class="accordion-toggle" ng-click="toggleOpen()" accordion-transclude="heading">
                      <span>Légalisation des pratiques illégales </span>
                      <span style="float:right" class="glyphicon glyphicon-plus " aria-hidden="true"></span>
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse">
                  <div class="panel-body" ng-transclude="">
                    <accordion-heading></accordion-heading>
                    <p>Le gouvernement a décidé de légaliser sans argument et sans contrôle les pratiques illégales des services de renseignement. L'étude d'impact du projet de loi et la communication du gouvernement ne justifient jamais cette légalisation
                      massive, extra-judiciaire et quasiment sans contrôle de la surveillance.</p>
                    <p>
                      <strong>Nous ne pouvons accepter sans contrôle une légalisation massive des pratiques des services de renseignement !</strong>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </accordion>
          <p class="link">
            &gt; <a href="http://wiki.laquadrature.net/Amender_le_PJL_Renseignement">Plus d’informations sur le Projet de loi</a></p>
          <p class="link">
            &gt; <a href="https://wiki.laquadrature.net/PJL_renseignement_campagne">Passez le mot !</a></p>
        </div>
      </div>
      <!-- ngInclude: 'app/main/components/deputes/deputes.html' -->
      <div>
        <div id="deputes" ng-controller="DeputesCtrl">
          <h1 class="container">Refusez cette loi de surveillance : Appelez vos députés</h1>
          <div class="container-fluid">
            <div class="row">
              <nav style="text-align: center;">
                <a class="btn btn-default <?php echo ($position == 2) ? 'active' : '' ?>" href="?all=1&amp;position=2#deputes">Favorables au texte</a>
                <a class="btn btn-default <?php echo ($position == 1) ? 'active' : '' ?>" href="?all=1&amp;position=1#deputes">Opposé·e·s au texte</a>
              </nav>
              <div id="deputes-list" class="col-md-12">
                <!-- ngRepeat: depute in filteredDeputees -->
                <?php
                  if (!$all) {
                    $deputes_chunk = array_chunk($deputes, $nppage);
                    $deputes = $deputes_chunk[$page-1];
                    if (!$deputes) $deputes = $deputes_chunk[0];
                  }

                  foreach($deputes as $depute):
                ?>
                <div class="depute-image-block">
                  <div class="depute-image">
                      <figure class="mask <?=$depute->color?> fade-out" fade-on-load="">
                        <img src="assets/deputes/<?=$depute->depute->slug?>.png" ng-src="assets/deputes/gilles-lurton.png">
                      </figure>
                      <div class="depute-name ng-binding"><?=$depute->depute->nom?></div>
                  </div>
                  <div class="depute-infos-static">
                    <h4><?=$depute->depute->nom?><small> <?=$depute->depute->groupe_sigle?></small></h4>
                    <ul>
                      <?php if($depute->infos->twitter): ?>
                      <li>
                        <a href="https://twitter.com/<?=$depute->infos->twitter?>">@<?=$depute->infos->twitter?></a>
                      </li>
                      <?php endif; ?>
                      <li>
                        <a href="mailto:<?=$depute->depute->emails[0]->email?>"><?=$depute->depute->emails[0]->email?></a>
                      </li>
                    </ul>
                    <p>

                    <?php if ($depute->infos->id_piphone): ?>
                    <p>
                      <a class="btn btn-success"
                      href="https://piphone.lqdn.fr/campaign/call2/pjl_renseignement/<?=$depute->infos->id_piphone?>/#mep" target="_blank" role="button">
                      Appelez gratuitement</a>
                    </p>
                    <?php endif; ?>
                  </div>
                </div>
                <?php
                  endforeach;
                ?>
              </div>
            </div>
            <div class="clear"></div>
            <nav>
              <ul class="pager">
                <li><a href="?page=<?=($page - 1)?>#deputes">Précédent</a></li>
                <li><a href="?page=<?=($page + 1)?>#deputes">Suivant</a></li>
              </ul>
            </nav>
            <p class="link container">
              &gt; <a href="https://pad.lqdn.fr/p/PJLdeputes">Mettez à jour les positions des députés</a></p>
          </div>
        </div>
      </div>
      <h1 class="container">Qui s’y oppose ?</h1>
      <div style="text-align: center">
        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WZOPKkqYFlA?list=PLBSDR8jbeRXPnaKCBgIuuznld9e1yHOky" frameborder="0" allowfullscreen></iframe>
      </div>
      <!-- ngInclude: 'app/main/components/oppositions/oppositions.html' -->
      <div class="container">
        <div id="oppositions-block" ng-controller="OppositionsCtrl">
          <!-- ngRepeat: opposition in oppositions -->
          <div style="" class="opposition thumbnail " ng-repeat="opposition in oppositions">
            <div class="caption">
              <div class="logo">
                <img src="assets/logos/creiss.jpg" alt="CREIS-Terminal">
              </div>
              <h3>CREIS-Terminal</h3>
              <p>CREIS-Terminal, composée principalement de chercheurs et d’enseignants de disciplines variées (droit, économie, gestion, informatique, psychologie, sociologie…) mène une réflexion sur les enjeux des Nouvelles Technologies de l’information
                et de la communication dans la société, en liaison avec sa revue Terminal.</p>
              <p class="opposition-link"><a href="http://www.lecreis.org/" class="btn btn-primary" role="button">Voir le site</a></p>
            </div>
          </div>
          <!-- end ngRepeat: opposition in oppositions -->
          <div style="" class="opposition thumbnail " ng-repeat="opposition in oppositions">
            <div class="caption">
              <div class="logo">
                <img src="assets/logos/laquadrature.png" alt="La Quadrature du Net">
              </div>
              <h3>La Quadrature du Net</h3>
              <p>La Quadrature du Net (abrégé LQDN) est une association de défense des droits et libertés des citoyens sur Internet fondée en 2008. Elle intervient dans les débats concernant la liberté d’expression, le droit d’auteur, la régulation du secteur
                des télécommunications, ou encore le respect de la vie privée sur Internet.</p>
              <p class="opposition-link"><a href="https://www.laquadrature.net/fr/loi-renseignement-tous-surveilles" class="btn btn-primary" role="button">Voir le site</a></p>
            </div>
          </div>
          <!-- end ngRepeat: opposition in oppositions -->
          <div style="" class="opposition thumbnail " ng-repeat="opposition in oppositions">
            <div class="caption">
              <div class="logo">
                <img src="assets/logos/amnesty.jpg" alt="Amnesty International">
              </div>
              <h3>Amnesty International</h3>
              <p>Amnesty International est un mouvement mondial regroupant plus de 7 millions de personnes qui prennent l’injustice comme une affaire personnelle. Nous faisons campagne pour un monde où chacun peut se prévaloir de ses droits.
              </p>
              <p class="opposition-link"><a href="http://www.amnesty.fr/Nos-campagnes/Liberte-expression/Actualites/Surveillance-en-France-les-defenseurs-des-droits-humains-s-inquietent-14664" class="btn btn-primary" role="button">Voir le site</a></p>
            </div>
          </div>
          <!-- end ngRepeat: opposition in oppositions -->
          <div style="" class="opposition thumbnail " ng-repeat="opposition in oppositions">
            <div class="caption">
              <div class="logo">
                <img src="assets/logos/rsf.png" alt="Reporters Sans Frontière">
              </div>
              <h3>Reporters Sans Frontière</h3>
              <p>Reporters sans frontières (RSF) est une organisation non gouvernementale internationale reconnue d’utilité publique en France se donnant pour objectif la défense de la liberté de la presse et la protection des sources des journalistes.</p>
              <p
              class="opposition-link"><a href="http://fr.rsf.org/france-inquietude-des-organisations-des-25-03-2015,47726.html" class="btn btn-primary" role="button">Voir le site</a></p>
            </div>
          </div>
          <!-- end ngRepeat: opposition in oppositions -->
          <div style="" class="opposition thumbnail " ng-repeat="opposition in oppositions">
            <div class="caption">
              <div class="logo">
                <img src="assets/logos/ldh.png" alt="La Ligue des Droits de l’Homme">
              </div>
              <h3>La Ligue des Droits de l’Homme</h3>
              <p>Ligue des droits de l’homme ou LDH, est une association visant à l’observation, la défense et la promulgation des droits de l’homme au sein de la République française, dans tous les domaines de la vie publique.
              </p>
              <p class="opposition-link"><a href="http://www.ldh-france.org/" class="btn btn-primary" role="button">Voir le site</a></p>
            </div>
          </div>
          <!-- end ngRepeat: opposition in oppositions -->
          <div style="" class="opposition thumbnail " ng-repeat="opposition in oppositions">
            <div class="caption">
              <div class="logo">
                <img src="assets/logos/ffmc.png" alt="Fédération Française des Motards en Colère (FFMC)">
              </div>
              <h3>Fédération Française des Motards en Colère (FFMC)</h3>
              <p>La Fédération Française des Motards en Colère (FFMC) défend et représente les usagers de 2 et 3-roues motorisés tout en promouvant des valeurs de solidarité, d’égalité et de liberté.</p>
              <p class="opposition-link"><a href="http://www.ffmc.asso.fr/?lang=fr" class="btn btn-primary" role="button">Voir le site</a></p>
            </div>
          </div>
          <!-- end ngRepeat: opposition in oppositions -->
          <div style="" class="opposition thumbnail " ng-repeat="opposition in oppositions">
            <div class="caption">
              <div class="logo">
                <img src="assets/logos/dal.jpg" alt="Droit au Logement">
              </div>
              <h3>Droit au Logement</h3>
              <p>Droit au logement est une association française dont le but est de permettre aux populations les plus fragilisées (mal logés et sans-logis) d’avoir accès à un logement décent, en exerçant leur droit au logement.</p>
              <p class="opposition-link"><a href="http://droitaulogement.org/" class="btn btn-primary" role="button">Voir le site</a></p>
            </div>
          </div>
          <!-- end ngRepeat: opposition in oppositions -->
          <div style="" class="opposition thumbnail " ng-repeat="opposition in oppositions">
            <div class="caption">
              <div class="logo">
                <img src="assets/logos/saf.png" alt="Syndicat des avocats">
              </div>
              <h3>Le Syndicat des Avocats de France</h3>
              <p>Le Syndicat des avocats de France est une organisation syndicale française regroupant des avocats.</p>
              <p class="opposition-link"><a href="http://www.lesaf.org/accueil.html" class="btn btn-primary" role="button">Voir le site</a></p>
            </div>
          </div>

                              </div>
    </div>
    <hr>
    <div class="footer">Site hébergé par <a href="https://www.laquadrature.net/fr">La Quadrature du Net</a> - <a href="mailto:contact@laquadrature.net">contact@laquadrature.net</a> - <a href="https://git.laquadrature.net/la-quadrature-du-net/sous-surveillance">Source</a></div>
  </div>
</div>
<script src="noscript_files/vendor-da33bfb8.js"></script>
<script src="noscript_files/app-c2f90a06.js"></script>
</body>

</html>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
