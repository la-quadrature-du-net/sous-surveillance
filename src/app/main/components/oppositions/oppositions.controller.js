'use strict';

angular.module('soussurveillance')
  .controller('OppositionsCtrl', function ($scope, _, $http) {
    $http.get('assets/data/oppositions.json').success(function(data){
      $scope.oppositions = _.shuffle(data);
    });
  })
  ;
