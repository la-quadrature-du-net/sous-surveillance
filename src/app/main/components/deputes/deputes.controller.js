'use strict';

angular.module('soussurveillance')
  .controller('DeputesCtrl', function ($scope, $http, _, Circonscriptions, Groupes, $stateParams, $anchorScroll, $location) {
    var deputes = [];
    $scope.search = {
      'name': '',
      'circ': '',
      'group': '',
      'status': ''
    };

    var currentDepute = null;
    var hideCurrentDepute = function hideCurrentDepute() {
      if (currentDepute) {
        currentDepute.show = false;
      }
    };

          
    $scope.filteredDeputees = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 21;
    $scope.totalItems = 0;

    $scope.Circonscriptions = Circonscriptions;
    $scope.Groupes = Groupes;

    $http.get('assets/data/deputes_infos.json').success(function(deputesInfos){
      $http.get('assets/data/deputes_en_mandat.json').success(function(data){
        deputes = data.deputes;
        $scope.deputes = _.shuffle(data.deputes);
        for (var i = 0; i < $scope.deputes.length; i++) {
          var slug = $scope.deputes[i].depute.slug;
          $scope.deputes[i].infos = deputesInfos[slug];
          $scope.deputes[i].show = false;
        }
        $scope.totalItems = data.deputes.length;
        $scope.applyPaginate();
          
          if ($stateParams.param) {
              if ($stateParams.param === 'pour') {
                  $scope.search.status = '2';
              } else if ($stateParams.param === 'contre') {
                  $scope.search.status = '1';
              }
              $location.hash('deputes');
              $anchorScroll();
              $scope.applySearch();
          }
      });
    });

    $scope.selectDepute = function selectDepute(depute) {
      if (depute.show === true) {
        depute.show = false;
        currentDepute = null;
      } else {
        depute.show = true;
        hideCurrentDepute();
        currentDepute = depute;
      }
    };

    $scope.applySearch = function applySearch() {
        var filteredDeputes = deputes;
        if ($scope.search.group !== '') {
            filteredDeputes = _.filter(filteredDeputes, function(depute) {
                return depute.depute.groupe_sigle === $scope.search.group;
            });
        }
        
        if ($scope.search.status !== '') {
            filteredDeputes = _.filter(filteredDeputes, function(depute) {
                return depute.infos.position === parseInt($scope.search.status, 10);
            });
        }
        
        if ($scope.search.circ !== '') {
            filteredDeputes = _.filter(filteredDeputes, function(depute) {
                return depute.depute.nom_circo === $scope.search.circ;
            });
        }
        
        if ($scope.search.name !== '') {
            var search = $scope.search.name.toLowerCase();
            filteredDeputes = _.filter(filteredDeputes, function(depute) {
                return depute.depute.nom.toLowerCase().indexOf(search) > -1;
            });
        }
        
        $scope.deputes = _.sortBy(filteredDeputes, function(depute) { return depute.depute.nom_de_famille; });
        
        hideCurrentDepute();
        $scope.totalItems = $scope.deputes.length;
        $scope.applyPaginate();
        // console.log($scope.deputes);
    };

    $scope.applyPaginate = function applyPaginate() {
      var begin = (($scope.currentPage - 1) * $scope.numPerPage);
      var end = begin + $scope.numPerPage;

      $scope.filteredDeputees = $scope.deputes.slice(begin, end);
    };

    $scope.colorMask = function colorMask(depute) {
      var status = depute.infos.position;
      if(status === 0) {
        return 'unknown-mask';
      } else if (status === 1) {
        return 'green-mask';
      } else {
        return 'red-mask';
      }
    };

  });
