'use strict';

angular.module('soussurveillance')
  .controller('SenateursCtrl', function ($scope, $http, _, Circonscriptions, GroupesSenat, $stateParams, $anchorScroll, $location) {
    var senateurs = [];
    $scope.search = {
      'name': '',
      'circ': '',
      'group': '',
      'status': ''
    };

    var currentSenateur = null;
    var hideCurrentSenateur = function hideCurrentSenateur() {
      if (currentSenateur) {
        currentSenateur.show = false;
      }
    };

    $scope.filteredSenateurs = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 24;
    $scope.totalItems = 0;

    $scope.Circonscriptions = Circonscriptions;
    $scope.Groupes = GroupesSenat;

    $http.get('assets/data/senateurs_infos.json').success(function(senateursInfos){
      $http.get('assets/data/senateurs_en_mandat.json').success(function(data){
        senateurs = data.senateurs;
        $scope.senateurs = _.shuffle(data.senateurs);
        for (var i = 0; i < $scope.senateurs.length; i++) {
          var slug = $scope.senateurs[i].senateur.slug;
          $scope.senateurs[i].infos = senateursInfos[slug];
          $scope.senateurs[i].show = false;
        }
        $scope.totalItems = data.senateurs.length;
        $scope.applyPaginate();
          
          if ($stateParams.param) {
              if ($stateParams.param === 'pour') {
                  $scope.search.status = '2';
              } else if ($stateParams.param === 'contre') {
                  $scope.search.status = '1';
              }
              $location.hash('senateurs');
              $anchorScroll();
              $scope.applySearch();
          }
      });
    });

    $scope.selectSenateur = function selectSenateur(senateur) {
      if (senateur.show === true) {
        senateur.show = false;
        currentSenateur = null;
      } else {
        senateur.show = true;
        hideCurrentSenateur();
        currentSenateur = senateur;
      }
    };

    $scope.applySearch = function applySearch() {
        var filteredSenateurs = senateurs;
        if ($scope.search.group !== '') {
            filteredSenateurs = _.filter(filteredSenateurs, function(senateur) {
                return senateur.senateur.groupe_sigle === $scope.search.group;
            });
        }
        
        if ($scope.search.status !== '') {
            filteredSenateurs = _.filter(filteredSenateurs, function(senateur) {
                return senateur.infos.position === parseInt($scope.search.status, 10);
            });
        }
        
        if ($scope.search.circ !== '') {
            filteredSenateurs = _.filter(filteredSenateurs, function(senateur) {
                return senateur.senateur.nom_circo === $scope.search.circ;
            });
        }
        
        if ($scope.search.name !== '') {
            var search = $scope.search.name.toLowerCase();
            filteredSenateurs = _.filter(filteredSenateurs, function(senateur) {
                return senateur.senateur.nom.toLowerCase().indexOf(search) > -1;
            });
        }
        
        $scope.senateurs = _.sortBy(filteredSenateurs, function(senateur) { return senateur.senateur.nom_de_famille; });
        
        hideCurrentSenateur();
        $scope.totalItems = $scope.senateurs.length;
        $scope.applyPaginate();
        // console.log($scope.senateurs);
    };

    $scope.applyPaginate = function applyPaginate() {
      var begin = (($scope.currentPage - 1) * $scope.numPerPage);
      var end = begin + $scope.numPerPage;

      $scope.filteredSenateures = $scope.senateurs.slice(begin, end);
    };

    $scope.colorMask = function colorMask(senateur) {
      var status = senateur.infos.position;
      if(status === 0) {
        return 'unknown-mask';
      } else if (status === 1) {
        return 'green-mask';
      } else {
        return 'red-mask';
      }
    };

  });
