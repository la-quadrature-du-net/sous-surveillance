'use strict';

angular.module('soussurveillance')

.directive('fadeOnLoad', function() {
  return {
    restrict: 'A',
    link: function(scope, element) {
      element.addClass('fade-in');
      element.children('img').on('load', function() {
        element.addClass('fade-out');
        element.removeClass('fade-in');
      });
    }
  };
});
