'use strict';
/*global window: false */

angular.module('soussurveillance', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'timer'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/{param}',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });

    $urlRouterProvider.otherwise('/');
  })

  // expose underscore
  .constant('_', window._)

  .run(function ($rootScope) {
     $rootScope._ = window._;
  })
;
